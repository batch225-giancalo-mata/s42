// querySelector() method returns the first element that matches a selector.


const txtFirstName = document.querySelector('#txt-first-name');

const spanFullName = document.querySelector('#span-full-name');


// addEventListener() -method attaches an event handler to an element.
// keyup - A keyboard key is released after typing.


txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
});


/*
	Event Target Value:
	- event.target - gives you the element that triggered tha event
	- event.target.value - returns the element where the event occured.
*/


txtFirstName.addEventListener('keyup', (event) => {
	console.log(event.target);
	console.log(event.target.value)
});
